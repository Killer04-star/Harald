import asyncio

import discord
import random
import sqlite3

import datetime

from sqlite3 import Error

from discord.ext import commands





class MyClient(discord.Client):
    # Login
    global bot
    bot = commands.Bot(command_prefix="$")
    global bot_author
    bot_author = open("../private/author.txt", 'r', encoding="utf8").read()
    global commands_list
    commands_list = open("../rsc/commands.txt", 'r', encoding="utf8").read().split(";")
    global responds
    responds = open("../rsc/responds.txt", 'r', encoding="utf8").read().split(";")
    global sent
    sent = False
    global meme_counter
    meme_counter = 0
    global trigger_day

    async def on_ready(self):

        print("Ich habe mich eingeloggt. Beep Bop.")
        global auto_delete
        auto_delete = False
        global conn
        conn = self.create_connection(r'../private/info.db')
        self.create_tables(conn)
        global responds
        global trigger_day
        print("Please enter trigger day")
        (trigger_day) = int(input())
        if trigger_day < 1 or trigger_day > 7:
            trigger_day = 3
        print("The bot will be triggered on the " + str(trigger_day) + ". day of the week.")
        await MyClient.change_presence(self, activity=discord.Game(name=responds[64]))
        await asyncio.gather(self.check_date())

    # await self.check_date()

    # On_Message_Received
    async def on_message(self, message):
        try:
            global conn
            global commands_list
            global responds
            global descriptions
            descriptions = open("../rsc/command_description.txt", 'r', encoding="utf8").read().split(";")
            commands = commands_list

            if message.author == client.user:
                return
            if message.author.bot:
                return

            if message.content.lower() == commands[0]:
                embed = discord.Embed(title=responds[22], colour=discord.Colour(0xffffff),
                                      url="https://github.com/KILLER04-star/Harald/",
                                      description=responds[34])

                embed.set_author(name=responds[26], url="https://github.com/KILLER04-star/Harald")
                embed.set_footer(text=responds[29])

                embed.add_field(name=commands[12], value=responds[63])
                embed.add_field(name=responds[30], value=commands[2])
                embed.add_field(name=responds[31], value=commands[1])
                embed.add_field(name=responds[32], value=commands[3])
                embed.add_field(name=responds[33], value=commands[10])
                await message.channel.send(embed=embed)

            if message.content.lower().startswith("$help -mittwoch"):
                embed = discord.Embed(title=responds[35], colour=discord.Colour(0xffffff),
                                      url="https://github.com/KILLER04-star/Harald/",description=responds[36])

                embed.set_author(name=responds[26], url="https://github.com/KILLER04-star/Harald")
                embed.set_footer(text=responds[29])

                embed.add_field(name=responds[37],
                                value=commands[7]+"\n"+responds[38])
                embed.add_field(name=responds[39],
                                value=commands[6]+"\n"+responds[40])
                embed.add_field(name=responds[41],
                                value=responds[42]+"\n"+responds[43]+commands[11])
                embed.add_field(name=responds[33], value=commands[10])
                await message.channel.send(embed=embed)

            if message.content.lower().startswith("$help -roulette"):
                embed = discord.Embed(title=responds[48], colour=discord.Colour(0xffffff),
                                      url="https://github.com/KILLER04-star/Harald/",
                                      description=responds[49])

                embed.set_author(name=responds[26], url="https://github.com/KILLER04-star/Harald")
                embed.set_footer(text=responds[29])

                embed.add_field(name=responds[46],
                                value=commands[4]+"\n"+responds[50])
                embed.add_field(name=responds[33], value=commands[10])
                await message.channel.send(embed=embed)

            if message.content.lower().startswith("$help -koz"):
                embed = discord.Embed(title=responds[44], colour=discord.Colour(0xffffff),
                                      url="https://github.com/KILLER04-star/Harald/",
                                      description=responds[45])

                embed.set_author(name=responds[26], url="https://github.com/KILLER04-star/Harald")
                embed.set_footer(text=responds[29])

                embed.add_field(name=responds[46], value=commands[9]+responds[47])
                embed.add_field(name=responds[33], value=commands[10])
                await message.channel.send(embed=embed)

            if message.content.startswith("$roulette"):
                bid = message.content.split('!')[1]
                bid_param = -3

                if bid.lower() == "s":
                    bid_param = -1

                elif bid.lower() == "r":
                    bid_param = -2

                else:
                    try:
                        bid_param = int(bid)
                    except:
                        bid_param = -3

                if bid_param == -3:
                    await message.channel.send(str(responds[4]))
                    return
                result = random.randint(0, 36)

                if bid_param == -1:
                    won = result % 2 == 0 and not result == 0

                elif bid_param == -2:
                    won = result % 2 == 1

                else:
                    won = result == bid_param

                if won:
                    embed = discord.Embed(title=responds[51], colour=discord.Colour(0x50e3c2),
                                          url="https://github.com/KILLER04-star/Harald/",
                                          description=responds[52])

                    embed.set_author(name=responds[26], url="https://github.com/KILLER04-star/Harald")
                    embed.set_footer(text=responds[29])

                    await message.channel.send(embed=embed)

                else:
                    await message.channel.send(file=discord.File('../rsc/gestern.jpg'))

            elif message.content.lower().startswith("$private hilfe"):
                await message.author.send(str(responds[6]))

            elif message.content.lower().startswith("$bot -mittwoch -fire"):
                await message.channel.send(message.guild.roles[0], file=discord.File('../rsc/mittwoch.png'))

            elif message.content.lower().startswith("$stats"):
                counter = 0

                async for m in message.channel.history():

                    if m.content.startswith == '''Das ist ja gestern nicht so gut gelaufen ''' and m.author == client.user:
                        counter = counter + 1

                    print(str(counter))

            elif message.content.lower().startswith("$setzkanal"):

                Server_id = message.guild.id

                if self.server_already_existing(str(Server_id), conn) == 0:

                    self.insert_data(str(Server_id), str(message.channel.id), conn)
                    self.change_channel(str(Server_id), str(message.channel.id), conn)
                    embed = discord.Embed(title=responds[9], colour=discord.Colour(0xbd002),
                                          url="https://github.com/KILLER04-star/Harald/",
                                          description=responds[28])

                    embed.set_author(name=responds[26], url="https://github.com/KILLER04-star/Harald")
                    embed.set_footer(text=responds[9])

                    embed.add_field(name=responds[22], value=responds[20])
                    await message.channel.send(embed=embed)

                else:
                    try:
                        globals()
                        self.change_channel(str(Server_id), str(message.channel.id), conn)
                        embed = discord.Embed(title=responds[27], colour=discord.Colour(0xbd002),
                                              url="https://github.com/KILLER04-star/Harald/",
                                              description=responds[28])

                        embed.set_author(name=responds[26], url="https://github.com/KILLER04-star/Harald")
                        embed.set_footer(text=responds[29])

                        embed.add_field(name=responds[22], value=responds[20])
                        await message.channel.send(embed=embed)

                    except Exception as DB_Failure:
                       # await message.channel.send(str(responds[11]) + str(DB_Failure))
                        embed = discord.Embed(title=responds[25], colour=discord.Colour(0xd0021b),
                                          url="https://github.com/KILLER04-star/Harald/issues",
                                          description=responds[24])
                        embed.set_author(name=responds[26], url="https://github.com/KILLER04-star/Harald")
                        embed.set_footer(text=responds[23])

                        embed.add_field(name=responds[19], value=str(DB_Failure))
                        embed.add_field(name=responds[22], value=responds[20])
                        await message.channel.send(embed=embed)


            elif message.content.lower().startswith("$z"):
                file = open("../rsc/quotes.txt", 'r', encoding="utf8")
                messages = file.read().split(";")
                val = random.randint(0, len(messages) - 1)
                embed = discord.Embed(title=responds[53], colour=discord.Colour(0x3400ff),
                                      url="https://github.com/KILLER04-star/Harald/", description=messages[val])

                embed.set_author(name=responds[26], url="https://github.com/KILLER04-star/Harald")
                embed.set_footer(text=responds[29])
                await message.channel.send(embed=embed)

            elif message.content.lower().startswith("$koz"):
                bid = message.content.lower().split("!")[1]
                result = random.randint(0, 1)

                if str(bid) == str(result):
                    embed = discord.Embed(title=responds[51], colour=discord.Colour(0x50e3c2),
                                          url="https://github.com/KILLER04-star/Harald/",
                                          description=responds[52])

                    embed.set_author(name=responds[26], url="https://github.com/KILLER04-star/Harald")
                    embed.set_footer(text=responds[29])
                    await message.channel.send(embed=embed)
                else:
                    await message.channel.send(file=discord.File('../rsc/gestern.jpg'))

            elif message.content.lower().startswith("$about"):
                myid = '<@792004795703623691>'
                embed = discord.Embed(title=responds[33], colour=discord.Colour(0x32ff),
                                      url="https://github.com/KILLER04-star/Harald/",
                                      description=responds[12]+myid+responds[60])

                embed.set_author(name=responds[26], url="https://github.com/KILLER04-star/Harald")
                embed.set_footer(text=responds[29])

                embed.add_field(name=responds[58], value=responds[59])
                embed.add_field(name=responds[56], value=responds[57])

                await message.channel.send(embed=embed)

            elif message.content.lower().startswith("$kanal"):
                channel = self.get_channel_of_server(conn, message.guild.id)

                if channel is None:
                    embed = discord.Embed(title=responds[55], colour=discord.Colour(0xff0000),
                                          url="https://github.com/KILLER04-star/Harald/", description=responds[14])

                    embed.set_author(name=responds[26], url="https://github.com/KILLER04-star/Harald")
                    embed.set_footer(text=responds[29])

                    embed.add_field(name=responds[22], value=responds[20])
                    await message.channel.send(str(responds[14]))
                else:
                    embed = discord.Embed(title=responds[54], colour=discord.Colour(0xf3ff00),
                                          url="https://github.com/KILLER04-star/Harald/", description=str(client.get_channel(channel)))

                    embed.set_author(name=responds[26], url="https://github.com/KILLER04-star/Harald")
                    embed.set_footer(text=responds[29])
                    await message.channel.send(embed=embed)

            elif message.content.lower().startswith("$see_data") and bot_author == str(message.author):
                await self.send_meme()
            elif message.content.lower().startswith("$show_commands"):

                index = 0
                embed = discord.Embed(title=responds[61], colour=discord.Colour(0xffffff),
                                      url="https://github.com/KILLER04-star/Harald/",
                                      description=responds[62])

                embed.set_author(name=responds[26], url="https://github.com/KILLER04-star/Harald")
                embed.set_footer(text=responds[29])

                while index < 13:
                    embed.insert_field_at(name=commands[index], value=descriptions[index], index=index)
                    index += 1
                await message.channel.send(embed=embed)


        except Exception as e:
            embed = discord.Embed(title=responds[25], colour=discord.Colour(0xd0021b),
                                  url="https://github.com/KILLER04-star/Harald/issues",
                                  description=responds[24])

            embed.set_author(name=responds[26], url="https://github.com/KILLER04-star/Harald")
            embed.set_footer(text=responds[23])

            embed.add_field(name=str(responds[21]), value=str(e))
            embed.add_field(name=responds[22], value=responds[20])

            await message.channel.send(embed=embed)
            print(e)

    async def on_guild_join(self, guild):
            global commands_list
            commands = commands_list
            global responds
            global descriptions
            descriptions = open("../rsc/command_description.txt", 'r', encoding="utf8").read().split(";")
            index = 0
            embed = discord.Embed(title=responds[61], colour=discord.Colour(0xffffff),
                                  url="https://github.com/KILLER04-star/Harald/",
                                  description=responds[62])

            embed.set_author(name=responds[26], url="https://github.com/KILLER04-star/Harald")
            embed.set_footer(text=responds[29])

            while index < 13:
                embed.insert_field_at(name=commands[index], value=descriptions[index], index=index)
                index += 1
            await guild.text_channels[0].send(embed=embed)

    def create_connection(self, db_file):
        # opens a connection to a sqlite-database containing necessary
        # information about the servers, like in which channel the bot is supposed to send memes
        conn = None

        try:
            conn = sqlite3.connect(db_file)
            print(sqlite3.version)
            return conn
        except Error as e:
            print(e)
        return conn

    def create_tables(self, conn):
        try:
            c = conn.cursor()

            c.execute('''CREATE TABLE Info (Server_id text, Channel_id text)''')

            conn.commit()
        except Error as e:
            print(e)

    def insert_data(self, Server_id, Channel_id, conn):
        c = conn.cursor()

        c.execute('''INSERT INTO Info VALUES (''' + Server_id + ''',''' + Channel_id + ''')''')

        conn.commit()

    def server_already_existing(self, Server_id, conn):

        c = conn.cursor()
        server = (Server_id,)

        c.execute('SELECT * FROM Info WHERE Server_id=?', server)

        return len(c.fetchall())

    def unicode_to_letters(self, text):
        text.replace("Ã¼", "ü")
        text.replace("ÃŸ", "ß")
        print(text)
        return text

    def change_channel(self, Server_id, Channel_id, conn):
        c = conn.cursor()
        server = (Server_id,)

        c.execute('DELETE FROM Info WHERE Server_id=?', server)

        conn.commit()

        self.insert_data(Server_id, Channel_id, conn)

    def get_data(self):
        global conn

        c = conn.cursor()

        c.execute('SELECT * FROM Info')

        return c.fetchall()

    async def send_meme(self):
        data = self.get_data()

        index = 0

        for i in data:
            info = data[index]
            channel_id = int(str(info).replace("'", "").split(",")[1].replace("(", "").replace(")", ""))
            channel = client.get_channel(channel_id)
            server_id = int(str(info).replace("'", "").split(",")[0].replace("(", "").replace(")", ""))
            guild_role = client.get_guild(server_id).roles[0]
            await channel.send(guild_role, file=discord.File('../rsc/mittwoch.png'))
            index += 1

    def get_channel_of_server(self, conn, server_id):
        c = conn.cursor()
        id = (server_id,)

        c.execute('SELECT Channel_id FROM Info WHERE Server_id=?', id)
        result = int(str(c.fetchall()).replace("(", "").replace(")", "").replace(",", "").replace("'", "").replace("[",
                                                                                                                   "").replace(
            "]", ""))
        return result

    async def check_date(self):
        global meme_counter
        day = str(datetime.datetime.today().weekday())
        global trigger_day
        if day == str(trigger_day - 1) and meme_counter == 0:
            await self.send_meme()
            meme_counter += 1
        elif day != str(trigger_day - 1):
            meme_counter = 0
        await asyncio.sleep(15)
        await asyncio.gather(self.check_date())

client = MyClient()
client.run(
    "")  # Manuell das Token einfügen | vor jedem Commit und Jedem Push unbedingt entfernen!
